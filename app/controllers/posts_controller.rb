require 'open-uri'
require 'rb-readline'
require 'byebug'

class PostsController < ApplicationController

  def read_and_parse_json(url)
    JSON.load(open(url))
  end

  # In a backend application we should have authors_controller and call the authors from there.
  # For this small example purpose we will call it here.
  def index
    @posts = read_and_parse_json("http://maqe.github.io/json/posts.json")
    @authors = read_and_parse_json("http://maqe.github.io/json/authors.json")
  end
end
